public class Second extends First
{
    public String name() { return "Second";}

    public void whoRules()
    {
        System.out.format("%s rules", super.name());
        System.out.format(" but %s is even better%n", name());
    }
}
