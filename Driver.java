public class Driver
{
    public static void main(String[] args)
    {
        System.out.format("Java version %s%n", getJavaVersion());
        Second s = new Second();
        Third t = new Third();

        s.whoRules();
        t.whoRules();
    }

    private static String getJavaVersion()
    {
        Runtime.Version runTimeVersion = Runtime.version();
        return String.format("%s.%s.%s.%s", runTimeVersion.feature(), runTimeVersion.interim(), runTimeVersion.update(), runTimeVersion.patch());
    }
}
